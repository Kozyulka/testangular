app.factory('Data', function () {

  function getData(dataSize) {
    // request data from server
    return makeRequest(dataSize);
  }

  function makeRequest(dataSize) {
    var columns = { id: "Идентификатор", name: "Название", price: "Стоимость", quantity: "Количество" };
    var data = [columns];
    var dataLength = parseInt(dataSize);

    //generate test data
    for (var i = 0; i < dataLength; i++) {
      var id = i + 1;
      data.push([id, 'Product ' + id, Math.round(Math.random() * 100), Math.round(Math.random() * 10)]);
    }

    return data;
  }

  return {
    getData: getData
  }

});
