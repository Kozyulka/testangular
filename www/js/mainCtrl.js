
app.controller('MainCtrl', function ($scope, Data) {

  $scope.columns = [];
  $scope.allData = [];
  $scope.dataSize = null;
  $scope.sortBy = null;
  var pageSize = 50;
  var page = 1;
  var maxPages = 0;
  $scope.visibleData = [];

  $scope.searchFilter = '';

  // request data from service
  $scope.getData = function (dataSize) {
    var data = Data.getData(dataSize);

    transformData(data);
  };

  function transformData(data) {
    var tableColumnsObject = data.splice(0, 1)[0];
    var tableRows = [];

    for (var i = 0; i < data.length; i++) {
      var row = {
        id: data[i][0],
        name: data[i][1],
        price: data[i][2],
        quantity: data[i][3]
      };

      tableRows.push(row);
    }

    $scope.columns = tableColumnsObject;
    $scope.allData = tableRows;

    page = 1;
    //calculate max pages number
    maxPages = Math.ceil(tableRows.length / pageSize);
    //show one page of table
    $scope.visibleData = $scope.allData.slice(page - 1, pageSize);
  }

  $scope.selectRow = function(row) {
    $scope.selectedRow = row;
  };

  $scope.sort = function (column) {
    //if already sorted by column => make reverse sorting
    if ($scope.sortBy === column) {
      $scope.sortBy = '-' + column;
    } else {
      $scope.sortBy = column;
    }
  };

  $scope.nextPage = function () {
    //check if next page is available
    if (maxPages > page) {
      page++;

      changePage();
    }
  };

  $scope.prevPage = function () {
    //check if previous page is available
    if (page >= 2) {
      page--;

      changePage();
    }
  };

  function changePage() {
    var indexFrom = (page - 1) * pageSize;

    $scope.visibleData = $scope.allData.slice(indexFrom, indexFrom + pageSize);
  }

});
